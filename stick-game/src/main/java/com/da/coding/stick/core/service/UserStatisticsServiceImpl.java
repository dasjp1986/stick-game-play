package com.da.coding.stick.core.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.da.coding.stick.infra.entity.GlobalStatistics;
import com.da.coding.stick.infra.entity.UserStatistics;
import com.da.coding.stick.infra.repository.UserStatisticsRepository;

@Service
public class UserStatisticsServiceImpl implements UserStatisticsService{

	@Autowired
	UserStatisticsRepository userStatisticsRepo;
	
	public UserStatistics saveUpdate(UserStatistics gameStatistics) {
		return userStatisticsRepo.save(gameStatistics);
	}

	public UserStatistics get(String userId) throws Exception {
		UserStatistics userStatistics = userStatisticsRepo.findOne(userId);
//		if(userStatistics==null){
//			throw new Exception("Unable to fetch any data for the user");
//		}
		return userStatistics;
	}

	public Collection<UserStatistics> getAll() {
		Pageable topTen= new PageRequest(0, 10);
		return (Collection<UserStatistics>) userStatisticsRepo.findAll(topTen);
	}

	public GlobalStatistics getGlobatStats() {
		return userStatisticsRepo.getgetGlobatStats();
	}

}
