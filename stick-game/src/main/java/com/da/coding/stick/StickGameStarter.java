package com.da.coding.stick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class StickGameStarter
//TODO remove comment to make deployable WAR
//extends SpringBootServletInitializer
{
	//TODO remove comment to make deployable WAR
	/*@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(StickGameStarter.class);
	}*/
	
    public static void main( String[] args )
    {
    	SpringApplication.run(StickGameStarter.class, args);
    }
}
