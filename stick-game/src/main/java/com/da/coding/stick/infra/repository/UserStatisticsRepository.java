package com.da.coding.stick.infra.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.da.coding.stick.infra.entity.GlobalStatistics;
import com.da.coding.stick.infra.entity.UserStatistics;

public interface UserStatisticsRepository extends CrudRepository<UserStatistics, String> {

	@Query("select new com.da.coding.stick.infra.entity.GlobalStatistics(SUM(userStats.won) as totalWin, SUM(userStats.lost) as totalLost,ROUND(SUM(userStats.won)*100.0/(SUM(userStats.lost)+SUM(userStats.won)),2) as lostRatio ,ROUND(SUM(userStats.lost)*100.0/(SUM(userStats.lost)+SUM(userStats.won)),2) as lostRatio) from UserStatistics userStats")
	GlobalStatistics getgetGlobatStats();

	@Query("select new com.da.coding.stick.infra.entity.UserStatistics(userStats.userID as userID,userStats.name as name,userStats.won as won ,userStats.lost as lost) from UserStatistics userStats order by 3 desc")
	List<UserStatistics> findAll(Pageable topTen);

}
