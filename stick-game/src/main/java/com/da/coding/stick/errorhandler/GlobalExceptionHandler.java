package com.da.coding.stick.errorhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

	Logger LOGGER= LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Some Interval Server")
	@ExceptionHandler(Throwable.class)
	public void defaultHandler(final Throwable th){
		LOGGER.debug("There is some internal server error", th);
	}
}
