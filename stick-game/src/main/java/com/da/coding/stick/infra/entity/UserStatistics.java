package com.da.coding.stick.infra.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userStats")
public class UserStatistics {
	
	@Id
	private String userID;
	private String name;
	private int won;
	private int lost;
	
	public UserStatistics() {

	}
	
	public UserStatistics(String userID, String name, int won, int lost) {
		super();
		this.userID = userID;
		this.name = name;
		this.won = won;
		this.lost = lost;
	}


	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public int getWon() {
		return won;
	}

	public void setWon(int won) {
		this.won = won;
	}

	public int getLost() {
		return lost;
	}

	public void setLost(int lost) {
		this.lost = lost;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
