package com.da.coding.stick.core.service;

import java.util.Collection;

import com.da.coding.stick.infra.entity.GlobalStatistics;
import com.da.coding.stick.infra.entity.UserStatistics;

public interface UserStatisticsService {
	UserStatistics saveUpdate(UserStatistics gameStatistics);

	UserStatistics get(String userId) throws Exception;
	
	Collection<UserStatistics> getAll();
	
	GlobalStatistics getGlobatStats();
}
